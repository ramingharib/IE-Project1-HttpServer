
import java.util.Hashtable;

/**
 * Created by ramingharib on 2/24/17.
 */
public class Calculator implements Do {

    @Override
    public String GetDo(Hashtable hashtable) {
        hashtable = (hashtable != null ? hashtable : null);
        if (hashtable.containsKey("n1") && hashtable.containsKey("op") && hashtable.containsKey("n2")) {
            String oprand = (String) hashtable.get("op");
            int Number1 = Integer.parseInt((String) hashtable.get("n1"));
            int Number2 = Integer.parseInt((String)hashtable.get("n2"));
            int Result = 0;
            switch (oprand) {
                case "+":
                    Result = Number1 + Number2;
                    break;
                case "-":
                    Result = Number1 - Number2;
                    break;
                case "*":
                    Result = Number1 * Number2;
                    break;
                case "/":
                    if(Number2 != 0)
                        Result = Number1 / Number2;
                    else {
                        Result = 0;
                    }
                    break;
                default:
                    Result = 0;
            }
            return Integer.toString(Result);
        }
        return "";

    }
}
