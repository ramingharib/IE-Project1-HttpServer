import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    private static ExecutorService threadPool = Executors.newCachedThreadPool();

    public static void main(String[] args) {
        Server server = new Server(9000);
        threadPool.execute(server);
        //new Thread(server).start();
        try {
            Thread.sleep(6000 * 1000);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
        System.out.println("Stopping Server");
        server.stop();
    }
}
