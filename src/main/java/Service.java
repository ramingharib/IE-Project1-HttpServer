import java.util.Hashtable;

/**
 * Created by ramingharib on 2/19/17.
 */
public class Service implements Do {


    public String GetDo(Hashtable hashtable) {
        hashtable = (hashtable != null ? hashtable:null);
        if(hashtable.containsKey("name") && hashtable.containsKey("family")){
            return "hi " + hashtable.get("name") +" "+ hashtable.get("family");
        }
        return "";
    }
}
