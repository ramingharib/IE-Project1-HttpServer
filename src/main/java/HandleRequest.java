/**
 * Created by ramingharib on 2/17/17.
 */

import java.io.*;
import java.lang.reflect.Method;
import java.net.*;
import java.util.Date;
import java.util.Hashtable;

public class HandleRequest implements Runnable {

    protected Socket clientSocket = null;
    protected String serverText = null;
    private OutputStream Output = null;
    private HttpParser HttpParser = null;

    public HandleRequest(Socket clientSocket, String serverText) {
        this.clientSocket = clientSocket;
        this.serverText = serverText;
    }

    public void run() {
        try {
            InputStream input = clientSocket.getInputStream();
            HttpParser = new HttpParser(input);
            int responseCode = HttpParser.parseRequest();
            String method = HttpParser.getMethod();
            if (method.equalsIgnoreCase("GET")) {
                String response = CreateResponse(responseCode);
                Output = clientSocket.getOutputStream();
                Output.write(response.getBytes());
                Output.close();
            } else if (method.equalsIgnoreCase("POST")) {
                Output = clientSocket.getOutputStream();
                HttpParser.parseBody();
                String response = CreateResponse(responseCode);
                Output.write(response.getBytes());
                Output.close();
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String CreateResponse(int responseCode) {
        Date date = new Date();
        String fileContent = "";

        if (responseCode == 400) {
            fileContent = ReturnFileContent("/ServerFiles/BadRequest.html");
            responseCode = 400;
        } else {
            if (HttpParser.getRequestURL().contains(".do")) {
                fileContent = ExecuteMethod();
            } else {
                fileContent = ReturnFileContent(HttpParser.getRequestURL());
            }
        }

        if (fileContent.isEmpty()) {
            fileContent = ReturnFileContent("/ServerFiles/NotFound.html");
            responseCode = 404;

            return ("HTTP/" + HttpParser.getVersion() + " " + HttpParser.getHttpReply(responseCode) + "\r\n" +
                    "Date: " + date.toString() + "\r\n" +
                    "Server: " + System.getProperty("os.name") + "\r\n" +
                    "Content-Length: " + fileContent.length() + "\r\n" +
                    "Content-Type: text/html;" + "\r\n" +
                    "Connection: Closed" + "\r\n" +
                    "\r\n" +
                    fileContent)
                    ;
        } else {
            return ("HTTP/" + HttpParser.getVersion() + " " + HttpParser.getHttpReply(responseCode) + "\r\n" +
                    "Date: " + date.toString() + "\r\n" +
                    "Server: " + System.getProperty("os.name") + "\r\n" +
                    "Content-Length: " + fileContent.length() + "\r\n" +
                    "Content-Type: text/html;" + "\r\n" +
                    "Connection: Closed" + "\r\n" +
                    "\r\n" +
                    fileContent)
                    ;
        }
    }

    private String ExecuteMethod() {
        Hashtable params = null;
        Class[] hashParam = new Class[1];
        hashParam[0] = Hashtable.class;
        String className = HttpParser.getRequestURL().substring(1).split(".do")[0];
        if(HttpParser.getMethod().equalsIgnoreCase("GET"))
            params = HttpParser.getParams();
        else if (HttpParser.getMethod().equalsIgnoreCase("POST")){
            params = HttpParser.getMessageBody();
        }
        try {
            Class cls = Class.forName(className);
            Object classObject = cls.newInstance();
            Method method = cls.getDeclaredMethod("GetDo", hashParam);
            Object value = method.invoke(classObject, params);
            return value.toString();
        } catch (Exception e) {
            return "";
        }
    }

    private String ReturnFileContent(String filePath) {
        String str = "";
        try {
            filePath = filePath.substring(1);
            File file = new File(filePath);
            if (!file.exists()) {
                str = "";
            } else {
                FileInputStream fis = new FileInputStream(file);
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
                str = new String(data, "UTF-8");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
